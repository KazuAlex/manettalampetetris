# La Manette à Lampe Tetris

Permet de jouer avec la Lampe Tetris de xurei avec des boutons dans le chat Twitch.

<div align="center">
![Aperçu](manettalampetetris.png)
</div>

## Installation

1. Télécharger [Greasemonkey pour Firefox](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) ou [Tampermonkey pour Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo), des plugins pour le navigateur permettant d'exécuter ses propres scripts sur une page web.
2. Ouvrir ensuite le script en cliquant sur ce lien : [`manettalampetetris.user.js`](https://gitlab.com/Desmu/manettalampetetris/-/raw/main/manettalampetetris.user.js), le plugin devrait automatiquement proposer son installation.
3. La Manette à Lampe Tetris devrait apparaître sous le chat des streams de [xurei](https://twitch.tv/xurei) et de [var_a](https://twitch.tv/var_a) et dans leurs versions popups (pour [xurei](https://www.twitch.tv/popout/xurei/chat) et [var_a](https://www.twitch.tv/popout/var_a/chat)).

## A propos de la Lampe Tetris

La Lampe Tetris est une lampe à l'effigie de blocs du jeu éponyme modifiée par [xurei](https://github.com/xurei) dans l'optique de permettre aux personnes présentes dans le chat de son stream Twitch de la contrôler.
Elle se compose d'une grille de huit LEDs sur sept éclairant chacune un bloc de couleur différente. En postant le message `!tetris ` suivi d'une suite de cinquante-six caractères étant `.` ou `1`, les personnes présentes dans le chat peuvent changer les LED éclairées de gauche à droite puis de haut en bas. `1` représente une LED allumée, `.` une LED éteinte.

Par exemple, pour afficher le motif `:-)` sur la lampe, il faut entrer le message `!tetris .....1...1....1.......1....11.1.......1..1....1......1..`. Ce message peut se décomposer ainsi :
```
!tetris
.....1..
.1....1.
......1.
...11.1.
......1.
.1....1.
.....1..
```

Retrouvez la Lampe Tetris régulièrement sur [le stream de xurei](https://www.twitch.tv/xurei).

## La Manette à Lampe Tetris

La Manette à Lampe Tetris est un script pour navigateur web de bureau greffant un ensemble de boutons à l'interface du chat Twitch ayant pour but de faciliter l'utilisation de la lampe.
Cinquante-six cases à cocher sont ajoutées au chat, reproduisant la forme de la grille. L'idée est de pouvoir plus facilement visualier un motif concevable avec la lampe avant de l'envoyer dans le chat. Ainsi, les cases cochées représentent les LEDs qui seront allumées.
Une fois la grille paramétrée comme souhaité, deux boutons sont disponibles : `Vider la grille` décoche toutes les cases, `Poster la grille comme motif` convertit l'état courant de la grille en message de chat et le poste.

Un troisième bouton `Poster le message du chat comme motif` est également disponible. Il récupère le message actuellement renseigné dans le champ texte dédié à son envoi, convertit chaque lettre (de A à Z, minuscules et majuscules, accents non inclus), chaque chiffre (de 0 à 9) et chaque espace en un motif pouvant être affiché dans la grille (tout caractère non supporté sera changé en espace), puis envoie une suite de commandes affichant le message lettre par lettre dans la lampe.
Par exemple, si le message `oui` est renseigné dans le champ texte (dans l'objectif que la lampe affiche `O`, puis `U`, puis `I`), cliquer sur `Poster le message du chat comme motif` enverra trois messages à la suite (avec une seconde d'écart entre chaque message) : `!tetris .111111.1......11......11......11......11......1.111111.`, `!tetris 1......11......11......11......11......11......1.111111.`, `!tetris ...11..............11......11......11......11......11...`, chacun représentant une lettre.
Ce mode étant susceptible de faire envoyer beaucoup de messages de chat en peu de temps, son utilisation est restreinte au chat de var_a, le bot de xurei réceptionnant également les commandes de son stream. Cela permet ainsi d'éviter de noyer le chat Twitch de xurei de commandes automatisées.
