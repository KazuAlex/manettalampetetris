// ==UserScript==
// @name        La Manette à Lampe Tetris
// @namespace   https://www.twitch.tv/
// @description Permet de jouer avec la lampe Tetris de xurei avec des boutons dans le chat Twitch.
// @include     https://www.twitch.tv/xurei
// @include     https://www.twitch.tv/popout/xurei/chat*
// @include     https://www.twitch.tv/var_a
// @include     https://www.twitch.tv/popout/var_a/chat*
// @version     1.0.1
// @grant       none
// @author      Desmu
// @homepageURL https://gitlab.com/Desmu/manettalampetetris
// @updateURL   https://gitlab.com/Desmu/manettalampetetris/-/raw/main/manettalampetetris.user.js
// @downloadURL https://gitlab.com/Desmu/manettalampetetris/-/raw/main/manettalampetetris.user.js
// ==/UserScript==

const debugChars = false;

const tetrisIcon = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMjE3LjcyNDk5bW0iCiAgIGhlaWdodD0iMjE3LjcyNDk5bW0iCiAgIHZpZXdCb3g9IjAgMCAyMTcuNzI0OTkgMjE3LjcyNDk5IgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc1IgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjEuMSAoM2JmNWFlMGQyNSwgMjAyMS0wOS0yMCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9ImRyYXdpbmcuc3ZnIgogICBpbmtzY2FwZTpleHBvcnQtZmlsZW5hbWU9IkM6XFVzZXJzXEFsZXhhbmRyZVxQaWN0dXJlc1xNYW5ldHRlVGV0cmlzXGRyYXdpbmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJuYW1lZHZpZXc3IgogICAgIHBhZ2Vjb2xvcj0iIzUwNTA1MCIKICAgICBib3JkZXJjb2xvcj0iI2VlZWVlZSIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0ibW0iCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIHNob3dib3JkZXI9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjAuNSIKICAgICBpbmtzY2FwZTpjeD0iOTEiCiAgICAgaW5rc2NhcGU6Y3k9IjQ2OSIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAwOSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMTkxMiIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJsYXllcjEiIC8+CiAgPGRlZnMKICAgICBpZD0iZGVmczIiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMDk1MCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZjhkOGQ7c3RvcC1vcGFjaXR5OjAuOTg0MzEzNzMiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3AxMDk0NiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2Y5MjgyODtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDEwOTQ4IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50OTk0OCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiM5MGZjODc7c3RvcC1vcGFjaXR5OjEiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5OTQ0IiAvPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojNGNlMjRjO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwLjQxMjE2MjM2IgogICAgICAgICBpZD0ic3RvcDk5NTAiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwOGM4MTI7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIGlkPSJzdG9wOTk0NiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgzNTYiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojNmM4NGZmO3N0b3Atb3BhY2l0eToxIgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODM1MiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzBjMDBlNjtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDgzNTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MDU5Ij4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2Y3YzNmYztzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDUwNTUiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNlZTYyZjk7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAuNDg4NDA5OTciCiAgICAgICAgIGlkPSJzdG9wODg3MiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2VhMTZmYTtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDUwNTciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMjg2LTEiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZWZlY2M4O3N0b3Atb3BhY2l0eTowLjk4NDMxMzczIgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wMzQ3OCIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmZTgwMztzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDM0ODAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPHJhZGlhbEdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ5OTQ4IgogICAgICAgaWQ9InJhZGlhbEdyYWRpZW50MjI2MCIKICAgICAgIGN4PSIxMTEuOTc1MTkiCiAgICAgICBjeT0iMTQ1LjI4MjQ2IgogICAgICAgZng9IjExMS45NzUxOSIKICAgICAgIGZ5PSIxNDUuMjgyNDYiCiAgICAgICByPSIzNi45NTQxNjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTEuNjQxMDAzNywtMS43OTgyODUxLDEuMjQzMjE5OCwtMS4xMzQ1MDU0LDUwLjc1ODY4NSw0OTAuNTQxMikiIC8+CiAgICA8cmFkaWFsR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDEwOTUwIgogICAgICAgaWQ9InJhZGlhbEdyYWRpZW50MjI2MC05IgogICAgICAgY3g9IjEyNC40ODI5MyIKICAgICAgIGN5PSIxNjQuMzYxNDMiCiAgICAgICBmeD0iMTI0LjQ4MjkzIgogICAgICAgZnk9IjE2NC4zNjE0MyIKICAgICAgIHI9IjM2Ljk1NDE2NiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgtMC44MTI0MTcyMiwtMC44NzIwNDg3MiwwLjgzNzY5Mjk1LC0wLjc4MDQxNTI2LDg3LjgwMTgxNywzMzUuMzgyNjQpIiAvPgogICAgPHJhZGlhbEdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQxMjg2LTEiCiAgICAgICBpZD0icmFkaWFsR3JhZGllbnQyMjYwLTktMiIKICAgICAgIGN4PSI5Ni44NTAzOTUiCiAgICAgICBjeT0iMTQ3LjkxODI3IgogICAgICAgZng9Ijk2Ljg1MDM5NSIKICAgICAgIGZ5PSIxNDcuOTE4MjciCiAgICAgICByPSIzNi45NTQxNjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTEuNTkyNjY4MSwtMS41MzA5NSwxLjAxMzg5OCwtMS4wNTQ3ODY1LDIxMi4wNTI5Myw0NDEuNzQ1NjYpIiAvPgogICAgPHJhZGlhbEdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ4MzU2IgogICAgICAgaWQ9InJhZGlhbEdyYWRpZW50MjI2MC05LTAiCiAgICAgICBjeD0iMTA0LjU3MjA3IgogICAgICAgY3k9IjE0My41MjQ4OSIKICAgICAgIGZ4PSIxMDQuNTcyMDciCiAgICAgICBmeT0iMTQzLjUyNDg5IgogICAgICAgcj0iMzYuOTU0MTY2IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KC0xLjAzMDcwNTksLTEuMDQ0OTI4NiwxLjA0MTYwNCwtMS4wMjc0MzYsODIuNjIwMDI4LDQ2MS41MzY5OSkiIC8+CiAgICA8cmFkaWFsR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDUwNTkiCiAgICAgICBpZD0icmFkaWFsR3JhZGllbnQyMjYwLTktMC0yIgogICAgICAgY3g9IjExMi42OTgwNyIKICAgICAgIGN5PSIxNTguNzUwNSIKICAgICAgIGZ4PSIxMTIuNjk4MDciCiAgICAgICBmeT0iMTU4Ljc1MDUiCiAgICAgICByPSIzNi45NTQxNjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTEuMDExNDAzMiwtMS4wNjM2MjMsMS4zMjAwNTM2LC0xLjI1NTI1ODQsMzYuMjQwNjQzLDU3MC43MTkwNSkiIC8+CiAgPC9kZWZzPgogIDxnCiAgICAgaW5rc2NhcGU6bGFiZWw9IkxheWVyIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNC4xNjg3NTUsLTc5Ljc4NTQxNikiPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTpub3JtYWw7ZmlsbDp1cmwoI3JhZGlhbEdyYWRpZW50MjI2MCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMzMzMzMzM7c3Ryb2tlLXdpZHRoOjU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmUiCiAgICAgICBpZD0icmVjdDMxIgogICAgICAgd2lkdGg9IjcwLjkwODMzMyIKICAgICAgIGhlaWdodD0iNzAuOTA4MzMzIgogICAgICAgeD0iMTYuNjY4NzU1IgogICAgICAgeT0iODIuMjg1NDE2IgogICAgICAgcnk9IjcuNjcyOTE2OSIKICAgICAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcQWxleGFuZHJlXFBpY3R1cmVzXE1hbmV0dGVUZXRyaXNcZHJhd2luZy5wbmciCiAgICAgICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgICAgICBpbmtzY2FwZTpleHBvcnQteWRwaT0iOTYiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im1peC1ibGVuZC1tb2RlOm5vcm1hbDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQyMjYwLTkpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMzMzMzMzO3N0cm9rZS13aWR0aDo1O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lIgogICAgICAgaWQ9InJlY3QzMS0xIgogICAgICAgd2lkdGg9IjcwLjkwODMzMyIKICAgICAgIGhlaWdodD0iNzAuOTA4MzMzIgogICAgICAgeD0iODcuNTc3MDg3IgogICAgICAgeT0iODIuMjg1NDE2IgogICAgICAgcnk9IjcuNjcyOTE2OSIKICAgICAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcQWxleGFuZHJlXFBpY3R1cmVzXE1hbmV0dGVUZXRyaXNcZHJhd2luZy5wbmciCiAgICAgICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgICAgICBpbmtzY2FwZTpleHBvcnQteWRwaT0iOTYiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im1peC1ibGVuZC1tb2RlOm5vcm1hbDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQyMjYwLTktMCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMzMzMzMzM7c3Ryb2tlLXdpZHRoOjU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmUiCiAgICAgICBpZD0icmVjdDMxLTEtNCIKICAgICAgIHdpZHRoPSI3MC45MDgzMzMiCiAgICAgICBoZWlnaHQ9IjcwLjkwODMzMyIKICAgICAgIHg9Ijg3LjU3NzA4IgogICAgICAgeT0iMTUzLjE5Mzc2IgogICAgICAgcnk9IjcuNjcyOTE2OSIKICAgICAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcQWxleGFuZHJlXFBpY3R1cmVzXE1hbmV0dGVUZXRyaXNcZHJhd2luZy5wbmciCiAgICAgICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgICAgICBpbmtzY2FwZTpleHBvcnQteWRwaT0iOTYiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im1peC1ibGVuZC1tb2RlOm5vcm1hbDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQyMjYwLTktMC0yKTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzMzMzMzMztzdHJva2Utd2lkdGg6NTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIKICAgICAgIGlkPSJyZWN0MzEtMS00LTkiCiAgICAgICB3aWR0aD0iNzAuOTA4MzMzIgogICAgICAgaGVpZ2h0PSI3MC45MDgzMzMiCiAgICAgICB4PSI4Ny41NzcwOCIKICAgICAgIHk9IjIyNC4xMDIwOCIKICAgICAgIHJ5PSI3LjY3MjkxNjkiCiAgICAgICBpbmtzY2FwZTpleHBvcnQtZmlsZW5hbWU9IkM6XFVzZXJzXEFsZXhhbmRyZVxQaWN0dXJlc1xNYW5ldHRlVGV0cmlzXGRyYXdpbmcucG5nIgogICAgICAgaW5rc2NhcGU6ZXhwb3J0LXhkcGk9Ijk2IgogICAgICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9Ijk2IiAvPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTpub3JtYWw7ZmlsbDp1cmwoI3JhZGlhbEdyYWRpZW50MjI2MC05LTIpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMzMzMzMzO3N0cm9rZS13aWR0aDo1O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lIgogICAgICAgaWQ9InJlY3QzMS0xLTYiCiAgICAgICB3aWR0aD0iNzAuOTA4MzMzIgogICAgICAgaGVpZ2h0PSI3MC45MDgzMzMiCiAgICAgICB4PSIxNTguNDg1NDEiCiAgICAgICB5PSI4Mi4yODU0MTYiCiAgICAgICByeT0iNy42NzI5MTY5IgogICAgICAgaW5rc2NhcGU6ZXhwb3J0LWZpbGVuYW1lPSJDOlxVc2Vyc1xBbGV4YW5kcmVcUGljdHVyZXNcTWFuZXR0ZVRldHJpc1xkcmF3aW5nLnBuZyIKICAgICAgIGlua3NjYXBlOmV4cG9ydC14ZHBpPSI5NiIKICAgICAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIgLz4KICA8L2c+Cjwvc3ZnPgo=";

const tetrisChars = {
    "0": "..1111...1....1..1....1..1....1..1....1..1....1...1111..",
    "1": "....1......11.....1.1.......1.......1.......1.......1...",
    "2": ".111111.1......1.....11....11....11.....1.......11111111",
    "3": ".111111.1......1.......1..11111........11......1.111111.",
    "4": ".....1......11.....1.1....1..1...1111111.....1.......1..",
    "5": "111111111.......1.......1111111........11......1.111111.",
    "6": ".111111.1......11.......1111111.1......11......1.111111.",
    "7": "11111111......11.....11.....11....11.....11.....11......",
    "8": ".111111.1......11......1.111111.1......11......1.111111.",
    "9": ".111111.1......11......1.1111111.......11......1.111111.",
    " ": "........................................................",
    "a": ".111111.1......11......1111111111......11......11......1",
    "b": "1111111.1......11......11111111.1......11......11111111.",
    "c": ".111111.1......11.......1.......1.......1......1.111111.",
    "d": "1111111.1......11......11......11......11......11111111.",
    "e": "111111111.......1.......111111111.......1.......11111111",
    "f": "111111111.......1.......1111111.1.......1.......1.......",
    "g": ".11111111.......1.......1..1111.1......11......1.111111.",
    "h": "1......11......11......1111111111......11......11......1",
    "i": "...11..............11......11......11......11......11...",
    "j": "......1...............1.......1.......1..1....1...1111..",
    "k": "1.....111...11..1.11....11......1.11....1...11..1.....11",
    "l": "1.......1.......1.......1.......1.......1.......11111111",
    "m": "1......111....111.1..1.11..11..11......11......11......1",
    "n": "11.....1111....11.11...11..11..11...11.11....1111.....11",
    "o": ".111111.1......11......11......11......11......1.111111.",
    "p": "1111111.1......11......11111111.1.......1.......1.......",
    "q": ".111111.1......11......11...1..11....1.11.....11.1111111",
    "r": "1111111.1......11......11111111.1.11....1...11..1.....11",
    "s": ".111111.1......11........111111........11......1.111111.",
    "t": "1111111111111111...11......11......11......11......11...",
    "u": "1......11......11......11......11......11......1.111111.",
    "v": "1......11......1.1....1..1....1...1..1....1..1.....11...",
    "w": "1......11......11......11..11..11.1..1.111....111......1",
    "x": "1......1.1....1...1..1.....11.....1..1...1....1.1......1",
    "y": "11....1111....11.11..11...1111.....11......11......11...",
    "z": "11111111.....11.....11.....11.....11.....11.....11111111"
};

function minifyString (str) {
    return str.trim().replace(/\r?\n|\r/g, " ").replace(/ {2}/g, "");
}

function sendMessage (message, sendable = true) {
    document.querySelector("[data-a-target='chat-input']").focus();
    Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value").set.call(document.querySelector("[data-a-target='chat-input']"), message);
    if (sendable === true) {
        document.querySelector("[data-a-target='chat-input']").dispatchEvent(new Event("input", {"bubbles": true}));
        document.querySelector("[data-a-target='chat-send-button']").click();
        document.querySelector("[data-a-target='chat-input']").blur();
    }
}

function getStreamerName () {
    const urlParams = window.location.href.split("/");
    if (urlParams[urlParams.length - 1] === "") {
        urlParams.pop();
    }
    let streamerName = "";
    if (window.location.href.indexOf("/popout/") === -1) {
        streamerName = urlParams[urlParams.length - 1];
    } else {
        streamerName = urlParams[urlParams.length - 2];
    }
    return streamerName;
}

function tetrisCreateStyle () {
    const styleTag = document.createElement("style");
    styleTag.innerText = minifyString(`
        .tetris-icon { cursor: pointer; display: inline; height: 35px; image-rendering: pixelated; margin-left: 5px; padding: 5px; width: 40px; }
        .tetris-button { background-color: var(--color-background-button-primary-default); border-radius: var(--border-radius-medium); color: var(--color-text-button-primary); cursor: pointer; font-weight: var(--font-weight-semibold); display: inline-block; margin: 5px; padding: 5px; }
        .tetris-button:hover, .tetris-button-focused { background-color: var(--color-background-button-primary-hover); }
        .tetris-hidden { display: none; }
        .tetris-cell { margin: 5px; }
    `);
    document.querySelector("head").append(styleTag);
}

function tetrisCreateInterface () {
    const imgTetrisIcon = document.createElement("img");
    imgTetrisIcon.classList.add("tetris-icon");
    imgTetrisIcon.setAttribute("src", `data:image/svg+xml;base64,${tetrisIcon}`);
    imgTetrisIcon.setAttribute("alt", "Manettatetris");
    document.querySelector(".chat-input__buttons-container > div:last-child").append(imgTetrisIcon);

    const div = document.createElement("div");
    div.classList.add("tetris", "tetris-hidden");
    let divHTML = minifyString(`
        <button type='button' class='tetris-grid-btn tetris-button'>Poster la grille comme motif</button>
        <button type='button' class='tetris-grid-clear-btn tetris-button'>Vider la grille</button><br>
    `);
    if (getStreamerName() === "var_a") {
        divHTML = `<button type='button' class='tetris-message-btn tetris-button'>Poster le message du chat comme motif</button>${divHTML}`;
    }
    for (let cellIndex = 0; cellIndex < 56; cellIndex += 1) {
        divHTML += "<input type='checkbox' class='tetris-cell'>";
        if (((cellIndex + 1) % 8) === 0) {
            divHTML += "<br>";
        }
    }
    if (debugChars === true) {
        Object.entries(tetrisChars).forEach(([normalChar, tetrisChar]) => {
            divHTML += `<button type='button' class='tetris-char-button tetris-button' data-tetris-char='${tetrisChar}'>${normalChar}</button>`;
        });
    }
    div.innerHTML = divHTML;
    document.querySelector(".chat-input").append(div);
}

function tetrisToggle () {
    document.querySelectorAll(".tetris").forEach((tag) => {
        tag.classList.toggle("tetris-hidden");
    });
}

function tetrisSendTextGrid () {
    const textToSend = document.querySelector("[data-a-target='chat-input']").value.toLowerCase();
    if (textToSend !== "") {
        let currCharIndex = 0;
        const textInterval = setInterval(() => {
            if (tetrisChars[Array.from(textToSend)[currCharIndex]]) {
                sendMessage(`!tetris ${tetrisChars[Array.from(textToSend)[currCharIndex]]}`);
            } else {
                sendMessage(`!tetris ${tetrisChars[" "]}`);
            }
            currCharIndex += 1;
            if (currCharIndex >= textToSend.length) {
                clearInterval(textInterval);
            }
        }, 1000);
    }
}

function tetrisSendGrid () {
    let tetrisGrid = "";
    document.querySelectorAll(".tetris-cell").forEach((cell) => {
        if (cell.checked) {
            tetrisGrid += "1";
        } else {
            tetrisGrid += ".";
        }
    });
    sendMessage(`!tetris ${tetrisGrid}`);
}

function tetrisClearGrid () {
    document.querySelectorAll(".tetris-cell").forEach((cell) => {
        cell.checked = false;
    });
}

window.addEventListener("load", () => {
    tetrisCreateStyle();
    tetrisCreateInterface();
    document.querySelector(".tetris-icon").addEventListener("click", () => {
        tetrisToggle();
    });
    if (document.querySelector(".tetris-message-btn")) {
        document.querySelector(".tetris-message-btn").addEventListener("click", () => {
            tetrisSendTextGrid();
        });
    }
    document.querySelector(".tetris-grid-btn").addEventListener("click", () => {
        tetrisSendGrid();
    });
    document.querySelector(".tetris-grid-clear-btn").addEventListener("click", () => {
        tetrisClearGrid();
    });
    if (debugChars === true) {
        document.querySelectorAll(".tetris-char-button").forEach((charBtn) => {
            charBtn.addEventListener("click", () => {
                Array.from(charBtn.getAttribute("data-tetris-char")).forEach((charBit, index) => {
                    if (charBit === "1") {
                        document.querySelectorAll(".tetris-cell")[index].checked = true;
                    } else if (charBit === ".") {
                        document.querySelectorAll(".tetris-cell")[index].checked = false;
                    }
                });
            });
        });
    }
});
